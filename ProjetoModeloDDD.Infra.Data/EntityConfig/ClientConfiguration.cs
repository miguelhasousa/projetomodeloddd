﻿using ProjetoModeloDDD.Domain.Entities;
using System;
using System.Data.Entity.ModelConfiguration;

namespace ProjetoModeloDDD.Infra.Data.EntityConfig
{
    public class ClientConfiguration : EntityTypeConfiguration<Cliente>
    {
        public ClientConfiguration()
        {
            HasKey(c => c.ClienteId);

            Property(c => c.Nome)
                .IsRequired()
                .HasMaxLength(150);
            Property(c => c.Sobrenome)
                .IsRequired()
                .HasMaxLength(150);
            Property(c => c.Email)
                .IsRequired();
        }
    }
}
